import pytest
import multitool.cmake_analysis
from pathlib import Path


@pytest.fixture()
def valid_cmake(tmpdir):
    cmake = """
    cmake_minimum_required( VERSION 3.0.3 FATAL_ERROR )

    project( "MyProject" VERSION 1.0.0 LANGUAGES CXX )
    add_executable(MyProject main.cpp)
    
    
    """
    path = tmpdir / "good.cmake"
    with open(path, "w+") as file:
        file.write(cmake)
    return path


@pytest.fixture()
def invalid_cmake(tmpdir):
    cmake = """cmake_minimum_required( VERSION 3.0.3 FATAL_ERROR )
    # COMMENT HERE
    project( "MyProject" VERSION 1.0.0 LANGUAGES CXX ) #COMMENT THERE
    add_definitions( -DTHIS_IS_BAD )
    add_executable(MyProject main.cpp)
    SET( CMAKE_CXX_FLAGS This_is_also_bad )
    """
    path = tmpdir / "bad.cmake"
    with open(path, "w+") as file:
        file.write(cmake)
    return path


@pytest.fixture()
def invalid_cmake_in_dir(tmpdir, invalid_cmake):
    path = (tmpdir / "invalid")
    path.mkdir()
    invalid_cmake = Path(invalid_cmake)
    dir_path = Path(invalid_cmake, path / "invalid.cmake")
    dir_path.write_text(invalid_cmake.read_text())
    return dir_path


class TestCMakeAnalysis:
    def test_evaluate_single_file_valid(self, valid_cmake):
        assert multitool.cmake_analysis.evaluate_file(str(valid_cmake)) == 0

    def test_evaluate_single_file_invalid(self, invalid_cmake):
        assert multitool.cmake_analysis.evaluate_file(str(invalid_cmake)) == 2

    def test_directory_recurse_file(self, tmpdir, valid_cmake, invalid_cmake, invalid_cmake_in_dir):
        assert multitool.cmake_analysis.evaluate_folder(str(tmpdir), True) == 4

    def test_directory_no_recurse_file(self, tmpdir, valid_cmake, invalid_cmake, invalid_cmake_in_dir):
        assert multitool.cmake_analysis.evaluate_folder(str(tmpdir), False) == 2
        assert multitool.cmake_analysis.evaluate_folder(str(invalid_cmake_in_dir.parent), False) == 2