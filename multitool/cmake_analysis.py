import re
import logging
import os
import argparse
import sys
from pathlib import Path


logger = logging.getLogger("CMakeAnalysis")
stream_handler = logging.StreamHandler()
stream_handler.setLevel(logging.INFO)
logger.addHandler(stream_handler)

_comment_regex = re.compile(r"^\s*#( *\S*)*")
_tokenizer = re.compile(r"(\S*)\s*\(\s*(\s*[^)]+\s*)\)")



_forbidden_methods = {
    "link_directories": "target_link_directories",
    "link_libraries": "target_link_libraries",
    "add_definitions": "add_compile_definitions or target_compile_options",
    "include_directories": "target_include_directories",
}


_forbidden_variables = {
    "CMAKE_CXX_FLAGS": "target_compile_features",
    "CMAKE_C_FLAGS": "target_compile_features"
}


_tokens = {}


def evaluate_folder(folder_path, recursive: bool = False):
    """
    Evaluates all the CMake files in a folder and outputs the results evaluated results.
    :param folder_path: Path to folder.
    :param recursive: If true, the subfolders will also be parsed.
    :return: Error count
    """
    error_count = 0
    for root, dirs, files in os.walk(folder_path):
        for file in files:
            if _is_cmake_file(file):
                error_count += evaluate_file(Path(root, file), folder_path + "/")
        if not recursive:
            break
    return error_count


def evaluate_file(file_path, file_root: str = "") -> int:
    """
    Evaluates a single file's CMake code.
    :param file_path: Path to file
    :param file_root: File root to be substracted from output (optional)
    :return: Error count
    """
    _parse_file(file_path, file_root)
    count = _evaluate_tokens()
    _clear_tokens()
    return count


def _parse_file(file_path, subs_root: str = ""):
    logger.info("Parsing {}".format(file_path))
    with open(file_path, "r") as file:
        file_path = str(file_path)[len(subs_root):]
        if file_path not in _tokens:
            _tokens[file_path] = []
        current_content = ""
        current_line_number = -1
        for index, line in enumerate(file):
            current_content, current_line_number = _parse_line(file_path, current_content, current_line_number, line,
                                                               index + 1)


def _is_cmake_file(file_name: str):
    return file_name.endswith('.cmake') or file_name == "CMakeLists.txt"


def _output_error(file_name: str, line_number: int, error_message: str, suggested_fix: str):
    logger.error(f"{file_name}@{line_number}: {error_message} - {suggested_fix}")


def _evaluate_forbidden_variable(file_name: str, line_number: int, token: str, content: str):
    error_count = 0
    if token.lower() != "set":
        return error_count

    set_variable = content.split()[0].strip()
    if set_variable in _forbidden_variables:
        error_count += 1
        _output_error(file_name, line_number, f"Forbidden variable {set_variable}",
                      f"Prefer {_forbidden_variables[set_variable]}")
    return error_count


def _evaluate_method(file_name: str, line_number: int, token: str) -> int:
    error_count = 0
    if token in _forbidden_methods:
        error_count += 1
        _output_error(file_name, line_number, f"Forbidden method {token}", f"Prefer {_forbidden_methods[token]}")
    return error_count


def _evaluate_tokens() -> int:
    error_count = 0
    for file_name, tokens in _tokens.items():
        for line, token, content in tokens:
            error_count += _evaluate_method(file_name, line, token)
            error_count += _evaluate_forbidden_variable(file_name, line, token, content)
    return error_count


def _clear_tokens():
    logger.debug("Clearing tokens")
    _tokens.clear()


def _parse_line(file_path: str, current_content: str, current_line_number: int, line: str, line_number: int):
    stripped_line = line.strip()
    if not stripped_line or _comment_regex.findall(stripped_line):
        return current_content, current_line_number
    current_content += line
    matches = _tokenizer.findall(current_content)
    if current_line_number == -1:
        current_line_number = line_number
    if matches:
        _tokens[file_path].append((current_line_number, matches[0][0].strip(), matches[0][1].strip()))
        current_line_number = -1
        current_content = ""

    return current_content, current_line_number


def _parse_arguments():
    parser = argparse.ArgumentParser(description='Parses CMake code and evaluates code quality. In case of errors, '
                                                 'it outputs the file name and line number of the bad practice, '
                                                 'alongside the recommended fix.')
    parser.add_argument("-f", "--file", type=str, action="store", dest="file_path", default="",
                        help="Path to a single file to parse. May be used instead of directory.")
    parser.add_argument("-d", "--dir", type=str, action="store", dest="directory", default="",
                        help="Path to a directory parse for .cmake or CMakeLists.txt. Overrides file parameter.")
    parser.add_argument("-r", "--recursive", action="store_true", dest="recursive", default=False,
                        help="When used with directory parsing, specifies that we should recursively look for files.")
    parser.add_argument("-o", "--output", type=str, action="store", dest="output_path", default="",
                        help="Path to output logs.")

    return parser.parse_args()


def main():
    args = _parse_arguments()

    if args.output_path:
        file_handler = logging.FileHandler(args.output_path)
        file_handler.setLevel(logging.INFO)
        logger.addHandler(file_handler)

    if args.directory:
        error_count = evaluate_folder(args.directory, args.recursive)
    elif args.file_path:
        if args.recursive:
            logger.warning("Recursive argument may only be used with directory parsing.")
        error_count = evaluate_file(args.file_path)
    else:
        logger.error("Invalid arguments. You must specify a directory or file to parse.")
        sys.exit(1)

    if error_count == 0:
        logger.info(f"Found no errors! Good job!")
    else:
        logger.error(f"Found a total of {error_count} errors.")
    sys.exit(error_count == 0)


if __name__ == "__main__":
    main()
